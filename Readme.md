# Cypress Parallel POC

## Introduction

In this repo I will be presenting different approaches to reduce timing when running cypress tests and also a way to improve coverage
in terms of browser execution


## Instalation

```
npm install
```

## Parallel Execution

The following command is going to execute all of the cypress specs in 5 threads.

More reference about cypress-parallel, please refer to this [Cypress Parallel](https://github.com/tnicola/cypress-parallel)

```
❯ cypress-parallel -s test -t 5 -d cypress/integration/examples/
```

## Cypress browser CLI argument

There is a way in Cypress to execute the specs passing it off a specific browser supported as an argument. See this as a reference [Cypress Browser](https://docs.cypress.io/guides/guides/launching-browsers.html#Browsers)

```
npx cypress run --browser chrome
```

## Gitlab + Cypress Dashboard

In Gitlab CI, we can configure parallel stages in the testing execution. Run this as a reference [Demo](https://gitlab.com/victor.arayal/cypress-parallel-sample/-/pipelineshttps://gitlab.com/victor.arayal/cypress-parallel-sample/-/pipelines) 

This approach is not open source and it requires a Cypress Plan to see the load-balancer distribution in Cypress Dashboard.
